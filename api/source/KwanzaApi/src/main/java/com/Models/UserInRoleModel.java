package com.Models;



import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Builder
@Getter
@Setter
@Component
public class UserInRoleModel {
	private UserAccountModel User;
	private UserRoleModel Role;
}
