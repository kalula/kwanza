package com.Models;



import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
@Builder
@Getter
@Setter
@AllArgsConstructor
@Component
public class CampaignModel {
	private int CampaignId;
	private String Name;
	private String Description;
	private String StartDate;
	private String Image;
	private String EndDate;
	private boolean Status;
	public CampaignModel() {}
}
