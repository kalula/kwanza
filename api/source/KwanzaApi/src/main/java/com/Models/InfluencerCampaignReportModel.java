package com.Models;

import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Builder
@Getter
@Setter
@Component
public class InfluencerCampaignReportModel {
	private int InfluencerCampaignId;
	private int Clicks;
	private int Impressions;
	private int Follows;

	private String CampaignName;
	private String CampaignDescription;
	private String CampaignStartDate;
	private String CampaignImage;
	private String CampaignEndDate;
	private boolean CampaignStatus;

	private String InfluencerFirstName;
	private String InfluencerMiddleName;
	private String InfluencerLastName;
	private String InfluencerAlias;
	
	public InfluencerCampaignReportModel() {}
}
