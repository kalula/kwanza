package com.Models;

import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Builder
@Getter
@Setter
@Component
public class InfluencerCampignModel {
	private int InfluencerCampaignId;
	private CampaignModel Campaign;
	private InfluencerModel Influencer;
	private int Clicks;
	private int Impressions;
	private int Follows;
	
	public InfluencerCampignModel() {}
}
