package com.Models;



import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;


@AllArgsConstructor
@Builder
@Getter
@Setter
@Component
public class InfluencerModel {
	private int InfluencerId;
	private UserAccountModel User;
	private int Followers;
	private Boolean Status;
	
	public InfluencerModel() {}
}
