package com.Models;




import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Builder
@Getter
@Setter
@Component
public class UserAccountModel {
	private int UserId;
	private String UserName;
	private String Email;
	private String Password;
	private String FirstName;
	private String MiddleName;
	private String LastName;
	private String Alias;
	private String DateCreated;
	private boolean IsActive;
	
	public UserAccountModel() {}
}
