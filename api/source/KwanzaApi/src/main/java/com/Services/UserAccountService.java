package com.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import com.Entities.UserAccount;
import com.Repositories.IUserAccountRepository;

@Component
public class UserAccountService {
@Autowired private IUserAccountRepository accountRepo;

public List<UserAccount> getAll()
{
	 List<UserAccount> accounts = accountRepo.findAll();
	 return accounts;
}
public List<UserAccount> add(UserAccount account)
{
	accountRepo.save(account);
	 List<UserAccount> accounts = accountRepo.findAll();
	 return accounts;
}
public UserAccount Get(Integer id)
{
	 Optional<UserAccount> account = accountRepo.findById(id);
	 return account.get();
}
}
