package com.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.Entities.UserRole;
import com.Repositories.IUserRoleRepository;

@Component
public class UserRolesService {
@Autowired private IUserRoleRepository roleRepo;

public List<UserRole> getAll()
{
	 List<UserRole> roles = roleRepo.findAll();
	 return roles;
}
public List<UserRole> add(UserRole role)
{
	roleRepo.save(role);
	 List<UserRole> roles = roleRepo.findAll();
	 return roles;
}
public UserRole GetCampaign(Integer id)
{
	 Optional<UserRole> role = roleRepo.findById(id);
	 return role.get();
}
}
