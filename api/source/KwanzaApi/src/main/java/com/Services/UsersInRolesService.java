package com.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.Entities.UserInRole;

import com.Repositories.IUsersInRolesRepository;

@Component
public class UsersInRolesService {
@Autowired private IUsersInRolesRepository roleRepo;

public List<UserInRole> getAll()
{
	 List<UserInRole> roles = roleRepo.findAll();
	 return roles;
}
public List<UserInRole> add(UserInRole role)
{
	roleRepo.save(role);
	 List<UserInRole> roles = roleRepo.findAll();
	 return roles;
}
public UserInRole GetCampaign(Integer id)
{
	 Optional<UserInRole> role = roleRepo.findById(id);
	 return role.get();
}
}
