package com.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.Entities.InfluencerCampaign;
import com.Repositories.IInfluencerCampaignRepository;

@Component
public class InfluencerCampaignService {
@Autowired private IInfluencerCampaignRepository influencerCampaignRepo;
public List<InfluencerCampaign> getAll()
{
	 List<InfluencerCampaign> campaigns = influencerCampaignRepo.findAll();
	 return campaigns;
}
public List<InfluencerCampaign> add(InfluencerCampaign exchangeValue)
{
	influencerCampaignRepo.save(exchangeValue);
	 List<InfluencerCampaign> campaigns = influencerCampaignRepo.findAll();
	 return campaigns;
}
public InfluencerCampaign GetCampaign(Integer id)
{
	 Optional<InfluencerCampaign> campaign = influencerCampaignRepo.findById(id);
	 return campaign.get();
}

}
