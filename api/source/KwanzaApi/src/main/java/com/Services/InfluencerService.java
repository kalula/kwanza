package com.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.Entities.Influencer;
import com.Repositories.IInfluencerRepository;

@Component
public class InfluencerService {
@Autowired private IInfluencerRepository influencerRepo;

public List<Influencer> getAll()
{
	 List<Influencer> influencers = influencerRepo.findAll();
	 return influencers;
}
public List<Influencer> add(Influencer exchangeValue)
{
	influencerRepo.save(exchangeValue);
	 List<Influencer> influencers = influencerRepo.findAll();
	 return influencers;
}
public Influencer Get(Integer id)
{
	 Optional<Influencer> influencer = influencerRepo.findById(id);
	 return influencer.get();
}
}
