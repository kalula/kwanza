package com.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.Entities.Campaign;
import com.Repositories.ICampaignRepository;


@Component
public class CampaignService {
@Autowired private ICampaignRepository campaignRepo;
public List<Campaign> getAll()
{
	 List<Campaign> campaigns = campaignRepo.findAll();
	 return campaigns;
}
public List<Campaign> add(Campaign campaign)
{
	campaignRepo.save(campaign);
	 List<Campaign> campaigns = campaignRepo.findAll();
	 return campaigns;
}
public Campaign Get(Integer id)
{
	 Optional<Campaign> campaign = campaignRepo.findById(id);
	 return campaign.get();
}
}
