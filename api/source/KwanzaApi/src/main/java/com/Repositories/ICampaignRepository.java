package com.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import com.Entities.Campaign;

@EnableJpaRepositories
@Repository
public interface ICampaignRepository extends JpaRepository<Campaign, Integer>{

}
