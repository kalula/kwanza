package com.controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.bind.annotation.RequestMapping;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@ComponentScan(basePackages="com")
@EnableAutoConfiguration
@EnableJpaRepositories(basePackages="com")
@EntityScan(basePackages="com")
@EnableSwagger2
public class KwanzaApiApplication extends SpringBootServletInitializer{
	@Override
	   protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
	      return application.sources(KwanzaApiApplication.class);
	}
	public static void main(String[] args) {
		
		SpringApplication.run(KwanzaApiApplication.class, args);
	}
	@RequestMapping(value = "/")
	   public String hello() {
	      return "Hello World from Tomcat";
	   }
}
