package com.Controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.Entities.Campaign;
import com.Entities.Influencer;
import com.Entities.InfluencerCampaign;
import com.Entities.UserAccount;
import com.Models.CampaignModel;
import com.Models.InfluencerCampaignReportModel;
import com.Models.InfluencerCampignModel;
import com.Models.InfluencerModel;
import com.Models.UserAccountModel;
import com.Services.CampaignService;
import com.Services.InfluencerCampaignService;
import com.Services.InfluencerService;
import com.Services.UserAccountService;

@RestController
public class InfluencerCampaignController {
	@Autowired
	InfluencerCampaignService influencerCampaignService;
	@Autowired
	InfluencerService influencerService;
	@Autowired
	UserAccountService userService;
	@Autowired
	CampaignService campaignService;

	@GetMapping("/influencercampaign")
	public String getMessage() {
		return "Welcome to Influencer Campaign Service";
	}

	@SuppressWarnings("null")
	@GetMapping("/influencercampaigns")
	public List<InfluencerCampignModel> GetAll() throws Exception {
		List<InfluencerCampignModel> influencerCampaigns = new ArrayList<InfluencerCampignModel>();
		try {
			List<InfluencerCampaign> models = influencerCampaignService.getAll();
			for (InfluencerCampaign influencerCampaign : models) {

				Campaign campaign = new Campaign();
				campaign = campaignService.Get(influencerCampaign.getCampaignId());
				Influencer influencer = new Influencer();
				influencer = influencerService.Get(influencerCampaign.getInfluencerId());
				UserAccount user = new UserAccount();
				user = userService.Get(influencer.getUserId());

				CampaignModel cModel = new CampaignModel();
				cModel.setCampaignId(campaign.getCampaignId());
				cModel.setDescription(campaign.getDescription());
				cModel.setEndDate(campaign.getEndDate().toString());
				cModel.setImage(campaign.getImage().toString());
				cModel.setName(campaign.getName());
				cModel.setStartDate(campaign.getStartDate().toString());
				cModel.setStatus(campaign.isStatus());

				InfluencerModel iModel = new InfluencerModel();
				iModel.setFollowers(influencer.getFollowers());
				iModel.setStatus(influencer.getStatus());

				UserAccountModel userModel = new UserAccountModel();
				userModel.setUserId(user.getUserId());
				userModel.setAlias(user.getAlias());
				userModel.setDateCreated(user.getDateCreated().toString());
				userModel.setEmail(user.getEmail());
				userModel.setFirstName(user.getFirstName());
				userModel.setMiddleName(user.getMiddleName());
				userModel.setLastName(user.getLastName());
				userModel.setUserName(user.getUserName());
				userModel.setIsActive(user.isIsActive());
				userModel.setPassword(user.getPassword());

				iModel.setUser(userModel);

				influencerCampaigns.add(new InfluencerCampignModel(influencerCampaign.getInfluencerId(), cModel, iModel,
						influencerCampaign.getClicks(), influencerCampaign.getImpressions(),
						influencerCampaign.getFollows()));
			}
		} catch (Exception e) {
			System.out.println(e.getMessage() + " " + e.getStackTrace().toString());
		}
		return influencerCampaigns;
	}

	@SuppressWarnings("null")
	@GetMapping("/influencercampaignsreport")
	public List<InfluencerCampaignReportModel> GetReport() throws Exception {
		List<InfluencerCampaignReportModel> influencerCampaigns = new ArrayList<InfluencerCampaignReportModel>();
		try {
			List<InfluencerCampaign> models = influencerCampaignService.getAll();
			for (InfluencerCampaign influencerCampaign : models) {

				Campaign campaign = new Campaign();
				campaign = campaignService.Get(influencerCampaign.getCampaignId());
				Influencer influencer = new Influencer();
				influencer = influencerService.Get(influencerCampaign.getInfluencerId());
				UserAccount user = new UserAccount();
				user = userService.Get(influencer.getUserId());

				CampaignModel cModel = new CampaignModel();
				cModel.setCampaignId(campaign.getCampaignId());
				cModel.setDescription(campaign.getDescription());
				cModel.setEndDate(campaign.getEndDate().toString());
				cModel.setImage(campaign.getImage().toString());
				cModel.setName(campaign.getName());
				cModel.setStartDate(campaign.getStartDate().toString());
				cModel.setStatus(campaign.isStatus());

				InfluencerModel iModel = new InfluencerModel();
				iModel.setFollowers(influencer.getFollowers());
				iModel.setStatus(influencer.getStatus());

				UserAccountModel userModel = new UserAccountModel();
				userModel.setUserId(user.getUserId());
				userModel.setAlias(user.getAlias());
				userModel.setDateCreated(user.getDateCreated().toString());
				userModel.setEmail(user.getEmail());
				userModel.setFirstName(user.getFirstName());
				userModel.setMiddleName(user.getMiddleName());
				userModel.setLastName(user.getLastName());
				userModel.setUserName(user.getUserName());
				userModel.setIsActive(user.isIsActive());
				userModel.setPassword(user.getPassword());
				iModel.setUser(userModel);
				
				
				InfluencerCampaignReportModel report = new InfluencerCampaignReportModel();
				report.setInfluencerCampaignId(influencerCampaign.getInfluencerCampaignId());
				report.setClicks(influencerCampaign.getClicks());
				report.setImpressions(influencerCampaign.getImpressions());
				report.setFollows(influencerCampaign.getFollows());
				report.setCampaignName(campaign.getName());
				report.setCampaignDescription(campaign.getDescription());
				report.setCampaignStartDate(campaign.getStartDate().toString());
				report.setCampaignImage(campaign.getImage().toString());
				report.setCampaignEndDate(campaign.getEndDate().toString());
				report.setCampaignStatus(campaign.isStatus());
				report.setInfluencerFirstName(user.getFirstName());
				report.setInfluencerMiddleName(user.getMiddleName());
				report.setInfluencerLastName(user.getLastName());
				report.setInfluencerAlias(user.getAlias());
				
				
				influencerCampaigns.add(report);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage() + " " + e.getStackTrace().toString());
		}
		return influencerCampaigns;
	}
	
	
	@SuppressWarnings("null")
	@PostMapping("/influencercampaigns")
	public List<InfluencerCampignModel> PostInfluencerCampaign(
			@RequestBody InfluencerCampignModel influencerCampaignModel) {
		List<InfluencerCampignModel> influencerCampaigns = new ArrayList<InfluencerCampignModel>();
		try {
			InfluencerCampaign iCampaign = new InfluencerCampaign();
			iCampaign.setImpressions(influencerCampaignModel.getImpressions());
			iCampaign.setCampaignId(influencerCampaignModel.getCampaign().getCampaignId());
			iCampaign.setInfluencerId(influencerCampaignModel.getInfluencer().getInfluencerId());
			iCampaign.setClicks(influencerCampaignModel.getClicks());
			iCampaign.setFollows(influencerCampaignModel.getFollows());

			List<InfluencerCampaign> models = influencerCampaignService.add(iCampaign);
			for (InfluencerCampaign influencerCampaign : models) {
				Campaign campaign = campaignService.Get(influencerCampaign.getCampaignId());
				Influencer influencer = influencerService.Get(influencerCampaign.getInfluencerId());
				UserAccount user = userService.Get(influencer.getUserId());

				CampaignModel cModel = new CampaignModel();
				cModel.setCampaignId(campaign.getCampaignId());
				cModel.setDescription(campaign.getDescription());
				cModel.setEndDate(campaign.getEndDate().toString());
				cModel.setImage(campaign.getImage().toString());
				cModel.setName(campaign.getName());
				cModel.setStartDate(campaign.getStartDate().toString());
				cModel.setStatus(campaign.isStatus());

				InfluencerModel iModel = new InfluencerModel();
				iModel.setFollowers(influencer.getFollowers());
				iModel.setStatus(influencer.getStatus());

				UserAccountModel userModel = new UserAccountModel();
				userModel.setUserId(user.getUserId());
				userModel.setAlias(user.getAlias());
				userModel.setDateCreated(user.getDateCreated().toString());
				userModel.setEmail(user.getEmail());
				userModel.setFirstName(user.getFirstName());
				userModel.setMiddleName(user.getMiddleName());
				userModel.setLastName(user.getLastName());
				userModel.setUserName(user.getUserName());
				userModel.setIsActive(user.isIsActive());
				userModel.setPassword(user.getPassword());

				iModel.setUser(userModel);

				influencerCampaigns.add(new InfluencerCampignModel(influencerCampaign.getInfluencerId(), cModel, iModel,
						influencerCampaign.getClicks(), influencerCampaign.getImpressions(),
						influencerCampaign.getFollows()));
			}
		} catch (Exception e) {

		}
		return influencerCampaigns;
	}

	@SuppressWarnings("null")
	@PostMapping("/influencercampaign-id")
	public InfluencerCampignModel PostInfluencerCampaign(@RequestBody Integer id) {
		InfluencerCampignModel influencerCampaign = new InfluencerCampignModel();
		try {

			InfluencerCampaign model = influencerCampaignService.GetCampaign(id);

			Campaign campaign = campaignService.Get(model.getCampaignId());
			Influencer influencer = influencerService.Get(model.getInfluencerId());
			UserAccount user = userService.Get(influencer.getUserId());

			CampaignModel cModel = new CampaignModel();
			cModel.setCampaignId(campaign.getCampaignId());
			cModel.setDescription(campaign.getDescription());
			cModel.setEndDate(campaign.getEndDate().toString());
			cModel.setImage(campaign.getImage().toString());
			cModel.setName(campaign.getName());
			cModel.setStartDate(campaign.getStartDate().toString());
			cModel.setStatus(campaign.isStatus());

			InfluencerModel iModel = new InfluencerModel();
			iModel.setFollowers(influencer.getFollowers());
			iModel.setStatus(influencer.getStatus());

			UserAccountModel userModel = new UserAccountModel();
			userModel.setUserId(user.getUserId());
			userModel.setAlias(user.getAlias());
			userModel.setDateCreated(user.getDateCreated().toString());
			userModel.setEmail(user.getEmail());
			userModel.setFirstName(user.getFirstName());
			userModel.setMiddleName(user.getMiddleName());
			userModel.setLastName(user.getLastName());
			userModel.setUserName(user.getUserName());
			userModel.setIsActive(user.isIsActive());
			userModel.setPassword(user.getPassword());

			iModel.setUser(userModel);

			influencerCampaign = new InfluencerCampignModel(model.getInfluencerId(), cModel, iModel, model.getClicks(),
					model.getImpressions(), model.getFollows());

		} catch (Exception e) {

		}
		return influencerCampaign;
	}
}
