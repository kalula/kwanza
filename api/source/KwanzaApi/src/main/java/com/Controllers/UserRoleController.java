package com.Controllers;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.Entities.UserRole;
import com.Models.UserRoleModel;
import com.Services.UserRolesService;

@RestController
public class UserRoleController {
	@Autowired
	UserRolesService roleService;

	@GetMapping("/userrole")
	public String getMessage() {
		return "Welcome to User Role Service";
	}

	@SuppressWarnings("null")
	@GetMapping("/userroles")
	public List<UserRoleModel> GetAll() {
		List<UserRoleModel> roles = new ArrayList<UserRoleModel>();
		try {
			List<UserRole> models = roleService.getAll();
			for (UserRole user : models) {

				UserRoleModel userModel = new UserRoleModel();
				userModel.setRoleId(user.getRoleId());
				userModel.setRoleName(user.getRoleName());

				roles.add(userModel);

			}
		} catch (Exception e) {
			
		}
		return roles;
	}

	@SuppressWarnings("null")
	@PostMapping("/userrolesadd")
	public List<UserRoleModel> PostUser(@RequestBody UserRoleModel role) {
		List<UserRoleModel> roles = new ArrayList<UserRoleModel>();
		try {

			UserRole myRole = new UserRole();
			myRole.setRoleId(role.getRoleId());
			myRole.setRoleName(role.getRoleName());

			List<UserRole> models = roleService.add(myRole);
			for (UserRole user : models) {

				UserRoleModel userModel = new UserRoleModel();
				userModel.setRoleId(user.getRoleId());
				userModel.setRoleName(user.getRoleName());

				roles.add(userModel);

			}
		} catch (Exception e) {

		}
		return roles;
	}

	@PostMapping("/userrole-id")
	public UserRoleModel PostUser(@RequestBody Integer id) {
		UserRoleModel userRole = new UserRoleModel();
		try {

			UserRole role = roleService.GetCampaign(id);
			userRole = new UserRoleModel();
			userRole.setRoleId(role.getRoleId());
			userRole.setRoleName(role.getRoleName());

		} catch (Exception e) {

		}
		return userRole;
	}
}
