package com.Controllers;


import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.Entities.Campaign;
import com.Models.CampaignModel;
import com.Services.CampaignService;

@RestController
public class CampaignController {
	@Autowired
	CampaignService campaignService;

	@GetMapping("/campaign")
	public String getMessage() {
		return "Welcome to Campaign Service";
	}

	@SuppressWarnings("null")
	@GetMapping("/campaigns")
	public List<CampaignModel> GetAll() {
		List<CampaignModel> campaigns = new ArrayList<CampaignModel>();
		try {
			List<Campaign> models = campaignService.getAll();
			for (Campaign campaign : models) {
				campaigns.add(new CampaignModel(campaign.getCampaignId(), campaign.getName(), campaign.getDescription(),
						campaign.getStartDate().toString(), campaign.getImage().toString(),
						campaign.getEndDate().toString(), campaign.isStatus()));
			}
		} catch (Exception e) {

		}
		return campaigns;
	}

	@SuppressWarnings("null")
	@PostMapping("/campaigns")
	public List<CampaignModel> PostCampaign(@RequestBody CampaignModel campaign) {
		List<CampaignModel> campaigns = new ArrayList<CampaignModel>();
		try {
			Campaign model = new Campaign(campaign.getCampaignId(), campaign.getName(), campaign.getDescription(),
					LocalDate.parse(campaign.getStartDate(), DateTimeFormatter.ofPattern("d/MM/yyyy")),
					campaign.getImage(),
					LocalDate.parse(campaign.getEndDate(), DateTimeFormatter.ofPattern("d/MM/yyyy")),
					campaign.isStatus());

			List<Campaign> models = campaignService.add(model);
			for (Campaign myCampaign : models) {
				campaigns.add(new CampaignModel(myCampaign.getCampaignId(), myCampaign.getName(),
						campaign.getDescription(), myCampaign.getStartDate().toString(), myCampaign.getImage(),
						myCampaign.getEndDate().toString(), myCampaign.isStatus()));
			}
		} catch (Exception e) {

		}
		return campaigns;
	}

	@PostMapping("/campaigns-id")
	public CampaignModel PostCampaign(@RequestBody Integer id) {
		CampaignModel campaign = new CampaignModel();
		try {

			Campaign model = campaignService.Get(id);
			campaign = new CampaignModel(model.getCampaignId(), model.getName(), model.getDescription(),
					model.getStartDate().toString(), model.getImage(), model.getEndDate().toString(), model.isStatus());

		} catch (Exception e) {

		}
		return campaign;
	}

}
