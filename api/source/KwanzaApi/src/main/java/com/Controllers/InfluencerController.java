package com.Controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.Entities.Influencer;
import com.Entities.UserAccount;
import com.Models.InfluencerModel;
import com.Models.UserAccountModel;
import com.Services.InfluencerService;
import com.Services.UserAccountService;

@RestController
public class InfluencerController {
	@Autowired
	InfluencerService influencerService;
	@Autowired
	UserAccountService userService;

	@GetMapping("/influencer")
	public String getMessage() {
		return "Welcome to Influencer Service";
	}

	@SuppressWarnings("null")
	@GetMapping("/influencers")
	public List<InfluencerModel> GetAll() {
		List<InfluencerModel> influencers = new ArrayList<InfluencerModel>();
		try {
			List<Influencer> models = influencerService.getAll();
			for (Influencer influencer : models) {

				int id = influencer.getUserId();
				UserAccount user = new UserAccount();
				user = userService.Get(id);
				UserAccountModel userModel = new UserAccountModel();
				userModel.setUserId(user.getUserId());
				userModel.setAlias(user.getAlias());
				userModel.setDateCreated(user.getDateCreated().toString());
				userModel.setEmail(user.getEmail());
				userModel.setFirstName(user.getFirstName());
				userModel.setMiddleName(user.getMiddleName());
				userModel.setLastName(user.getLastName());
				userModel.setUserName(user.getUserName());
				userModel.setIsActive(user.isIsActive());
				userModel.setPassword(user.getPassword());

				influencers.add(new InfluencerModel(influencer.getInfluencerId(), userModel, influencer.getFollowers(),
						influencer.getStatus()));
			}
		} catch (Exception e) {
			System.out.println(e.getMessage() + " " + e.getStackTrace().toString());
		}
		return influencers;
	}

	@SuppressWarnings("null")
	@PostMapping("/influencers")
	public List<InfluencerModel> PostCampaign(@RequestBody InfluencerModel influencer) {
		List<InfluencerModel> influencers = new ArrayList<InfluencerModel>();
		try {

			Influencer model = new Influencer(influencer.getInfluencerId(), influencer.getUser().getUserId(),
					influencer.getFollowers(), influencer.getStatus());

			List<Influencer> models = influencerService.add(model);
			for (Influencer myCampaign : models) {
				UserAccount user = userService.Get(myCampaign.getUserId());
				UserAccountModel userModel = new UserAccountModel();
				userModel.setUserId(user.getUserId());
				userModel.setAlias(user.getAlias());
				userModel.setDateCreated(user.getDateCreated().toString());
				userModel.setEmail(user.getEmail());
				userModel.setFirstName(user.getFirstName());
				userModel.setMiddleName(user.getMiddleName());
				userModel.setLastName(user.getLastName());
				userModel.setUserName(user.getUserName());
				userModel.setIsActive(user.isIsActive());
				userModel.setPassword(user.getPassword());
				influencers.add(new InfluencerModel(influencer.getInfluencerId(), userModel, influencer.getFollowers(),
						influencer.getStatus()));
			}
		} catch (Exception e) {

		}
		return influencers;
	}

	@PostMapping("/influencer-id")
	public InfluencerModel PostCampaign(@RequestBody Integer id) {
		InfluencerModel campaign = new InfluencerModel();
		try {

			Influencer model = influencerService.Get(id);
			UserAccount user = userService.Get(model.getUserId());
			UserAccountModel userModel = new UserAccountModel();

			userModel.setAlias(user.getAlias());
			userModel.setDateCreated(user.getDateCreated().toString());
			userModel.setEmail(user.getEmail());
			userModel.setFirstName(user.getFirstName());
			userModel.setMiddleName(user.getMiddleName());
			userModel.setLastName(user.getLastName());
			userModel.setUserName(user.getUserName());
			userModel.setIsActive(user.isIsActive());
			userModel.setPassword(user.getPassword());
			campaign = new InfluencerModel(model.getInfluencerId(), userModel, model.getFollowers(), model.getStatus());

		} catch (Exception e) {

		}
		return campaign;
	}
}
