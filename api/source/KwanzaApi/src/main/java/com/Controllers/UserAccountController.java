package com.Controllers;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.Entities.UserAccount;
import com.Models.UserAccountModel;
import com.Services.UserAccountService;

@RestController
public class UserAccountController {
	@Autowired
	UserAccountService userAccountService;

	@GetMapping("/useraccount")
	public String getMessage() {
		return "Welcome to User Account Service";
	}

	@SuppressWarnings("null")
	@GetMapping("/allaccounts")
	public List<UserAccountModel> GetAll() {
		List<UserAccountModel> userAccounts = new ArrayList<UserAccountModel>();
		try {
			List<UserAccount> models = userAccountService.getAll();
			for (UserAccount user : models) {

				UserAccountModel userModel = new UserAccountModel();
				userModel.setUserId(user.getUserId());
				userModel.setAlias(user.getAlias());
				userModel.setDateCreated(user.getDateCreated().toString());
				userModel.setEmail(user.getEmail());
				userModel.setFirstName(user.getFirstName());
				userModel.setMiddleName(user.getMiddleName());
				userModel.setLastName(user.getLastName());
				userModel.setUserName(user.getUserName());
				userModel.setIsActive(user.isIsActive());
				userModel.setPassword(user.getPassword());

				userAccounts.add(userModel);

			}
		} catch (Exception e) {

		}
		return userAccounts;
	}

	@SuppressWarnings("null")
	@PostMapping("/useraccountsadd")
	public List<UserAccountModel> PostUser(@RequestBody UserAccountModel userAccount) {
		List<UserAccountModel> userAccounts = new ArrayList<UserAccountModel>();
		try {

			UserAccount account = new UserAccount();
			account.setUserId(userAccount.getUserId());
			account.setAlias(userAccount.getAlias());
			account.setDateCreated(
					LocalDate.parse(userAccount.getDateCreated(), DateTimeFormatter.ofPattern("d/MM/yyyy")));
			account.setEmail(userAccount.getEmail());
			account.setFirstName(userAccount.getFirstName());
			account.setMiddleName(userAccount.getMiddleName());
			account.setLastName(userAccount.getLastName());
			account.setUserName(userAccount.getUserName());
			account.setIsActive(userAccount.isIsActive());
			account.setPassword(userAccount.getPassword());

			List<UserAccount> models = userAccountService.add(account);
			for (UserAccount user : models) {

				UserAccountModel userModel = new UserAccountModel();
				userModel.setUserId(user.getUserId());
				userModel.setAlias(user.getAlias());
				userModel.setDateCreated(user.getDateCreated().toString());
				userModel.setEmail(user.getEmail());
				userModel.setFirstName(user.getFirstName());
				userModel.setMiddleName(user.getMiddleName());
				userModel.setLastName(user.getLastName());
				userModel.setUserName(user.getUserName());
				userModel.setIsActive(user.isIsActive());
				userModel.setPassword(user.getPassword());

				userAccounts.add(userModel);

			}
		} catch (Exception e) {

		}
		return userAccounts;
	}

	@PostMapping("/useraccount-id")
	public UserAccountModel PostUser(@RequestBody Integer id) {
		UserAccountModel userAccount = new UserAccountModel();
		try {	

			UserAccount user = userAccountService.Get(id);

			userAccount = new UserAccountModel();
			userAccount.setUserId(user.getUserId());
			userAccount.setAlias(user.getAlias());
			userAccount.setDateCreated(user.getDateCreated().toString());
			userAccount.setEmail(user.getEmail());
			userAccount.setFirstName(user.getFirstName());
			userAccount.setMiddleName(user.getMiddleName());
			userAccount.setLastName(user.getLastName());
			userAccount.setUserName(user.getUserName());
			userAccount.setIsActive(user.isIsActive());
			userAccount.setPassword(user.getPassword());

		} catch (Exception e) {

		}
		return userAccount;
	}
}
