package com.Entities;

import java.io.InputStream;
import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;



import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="influencer_campaigns")
@AllArgsConstructor
@Builder
@Getter
@Setter

public class InfluencerCampaign implements Serializable{
	@Id
	@Column(name="influencer_campaign_id")
	private int InfluencerCampaignId;
	@Column(name="campaignid")
	private int CampaignId;
	@Column(name="influencerid")
	private int InfluencerId;
	@Column(name="clicks")
	private int Clicks;
	@Column(name="impresssions")
	private int Impressions;
	@Column(name="follows")
	private int Follows;

	public InfluencerCampaign() {}
}
