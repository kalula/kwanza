package com.Entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;



import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="influencers")
@AllArgsConstructor
@Builder
@Getter
@Setter

public class Influencer implements Serializable{
	@Id
	@Column(name="influencer_id")
	private int InfluencerId;
	@Column(name="userid")
	private int UserId;
	@Column(name="followers")
	private int Followers;
	@Column(name="status")
	private Boolean Status;
	
	public Influencer() {}
}
