package com.Entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;



import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="users_in_roles")
@AllArgsConstructor
@Builder
@Getter
@Setter

public class UserInRole implements Serializable{
	@Id
	@Column(name="userid")
	private int UserId;
	@Column(name="roleId")
	private int RoleId;
}
