package com.Entities;

import java.io.InputStream;
import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;



import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="campaigns")
@AllArgsConstructor
@Builder
@Getter
@Setter

public class Campaign implements Serializable{
	@Id
	@Column(name="campaign_id")
	private int CampaignId;
	@Column(name="name")
	private String Name;
	@Column(name="description")
	private String Description;
	@Column(name="start_date")
	private LocalDate StartDate;
	@Column(name="image")
	private String Image;
	@Column(name="end_date")
	private LocalDate EndDate;
	@Column(name="status")
	private boolean Status;
	
	public Campaign() {}
}
