package com.Entities;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="user_roles")
@AllArgsConstructor
@Builder
@Getter
@Setter
public class UserRole implements Serializable{
	@Id
	@Column(name="roleid")
	private int RoleId;
	@Column(name="role_name")
	private String RoleName;
	
	public UserRole() {}
}
