package com.Entities;
import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;



import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="user_accounts")
@AllArgsConstructor
@Builder
@Getter
@Setter
public class UserAccount implements Serializable {
@Id
@Column(name="userid")
private int UserId;
@Column(name="username")
private String UserName;
@Column(name="email")
private String Email;
@Column(name="password")
private String Password;
@Column(name="first_name")
private String FirstName;
@Column(name="middle_name")
private String MiddleName;
@Column(name="last_name")
private String LastName;
@Column(name="alias")
private String Alias;
@Column(name="date_created")
private LocalDate DateCreated;
@Column(name="is_active")
private boolean IsActive;

public UserAccount() {}
}
