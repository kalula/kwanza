-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 01, 2019 at 07:39 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kwanza`
--

-- --------------------------------------------------------

--
-- Table structure for table `campaigns`
--

CREATE TABLE `campaigns` (
  `campaign_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `start_date` date NOT NULL,
  `image` blob NOT NULL,
  `end_date` date NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `campaigns`
--

INSERT INTO `campaigns` (`campaign_id`, `name`, `description`, `start_date`, `image`, `end_date`, `status`) VALUES
(1, 'First', 'First Campaign', '2019-01-01', 0x737472696e67, '2019-12-31', 1);

-- --------------------------------------------------------

--
-- Table structure for table `influencers`
--

CREATE TABLE `influencers` (
  `influencer_id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `followers` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `influencers`
--

INSERT INTO `influencers` (`influencer_id`, `userid`, `followers`, `status`) VALUES
(1, 1, 56756756, 1);

-- --------------------------------------------------------

--
-- Table structure for table `influencer_campaigns`
--

CREATE TABLE `influencer_campaigns` (
  `influencer_campaign_id` int(11) NOT NULL,
  `campaign_id` int(11) NOT NULL DEFAULT '1',
  `influencer_id` int(11) NOT NULL DEFAULT '1',
  `clicks` int(11) NOT NULL,
  `impressions` int(11) NOT NULL DEFAULT '1',
  `follows` int(11) NOT NULL,
  `campaignid` int(11) DEFAULT NULL,
  `impresssions` int(11) DEFAULT '1',
  `influencerid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `influencer_campaigns`
--

INSERT INTO `influencer_campaigns` (`influencer_campaign_id`, `campaign_id`, `influencer_id`, `clicks`, `impressions`, `follows`, `campaignid`, `impresssions`, `influencerid`) VALUES
(1, 1, 1, 232344455, 1, 2323, 1, 12323, 1),
(2, 1, 1, 232344455, 1, 2323, 1, 12323, 1),
(3, 1, 1, 232344455, 1, 2323, 1, 12323, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users_in_roles`
--

CREATE TABLE `users_in_roles` (
  `userid` int(11) NOT NULL,
  `roleid` int(11) NOT NULL,
  `role_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_accounts`
--

CREATE TABLE `user_accounts` (
  `userid` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `date_created` date NOT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_accounts`
--

INSERT INTO `user_accounts` (`userid`, `username`, `email`, `password`, `first_name`, `middle_name`, `last_name`, `alias`, `date_created`, `is_active`) VALUES
(1, 'claudian', 'cl@yahoo.com', 'password', 'John', 'F', 'Doe', 'Claudian', '2016-12-12', 1),
(2, 'isultan', 'isultan@gmail.com', 'password', 'Idris', 'W', 'Sultan', 'Idris', '2017-12-12', 1),
(3, 'bbarnabas', 'bb@gmail.com', 'password', 'Barnabas', 'B', 'Barnabas', 'Barnaba', '2018-12-12', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `roleid` int(11) NOT NULL,
  `role_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`roleid`, `role_name`) VALUES
(1, 'Admin'),
(2, 'Influencer'),
(3, 'User');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `campaigns`
--
ALTER TABLE `campaigns`
  ADD PRIMARY KEY (`campaign_id`);

--
-- Indexes for table `influencers`
--
ALTER TABLE `influencers`
  ADD PRIMARY KEY (`influencer_id`);

--
-- Indexes for table `influencer_campaigns`
--
ALTER TABLE `influencer_campaigns`
  ADD PRIMARY KEY (`influencer_campaign_id`);

--
-- Indexes for table `user_accounts`
--
ALTER TABLE `user_accounts`
  ADD PRIMARY KEY (`userid`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`roleid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `campaigns`
--
ALTER TABLE `campaigns`
  MODIFY `campaign_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `influencers`
--
ALTER TABLE `influencers`
  MODIFY `influencer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `influencer_campaigns`
--
ALTER TABLE `influencer_campaigns`
  MODIFY `influencer_campaign_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_accounts`
--
ALTER TABLE `user_accounts`
  MODIFY `userid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `roleid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
