<?php
class Influencer{
    private $user;
    private $status;
    private    $influencerId;
    private     $followers;

    public function getUser() {
		return $this->user;
	}    
        public function getStatus() {
		return $this->status;
    }
            function getInfluencerId() {
		return $this->influencerId;
    }
    
        function getFollowers() {
		return $this->followers;
    }

    
    public function setUser($value) {
		 $this->user=$value;
	}
 
	public function setStatus($value) {
		 $this->status=$value;
    }
    public function setInfluencerId($value) {
		 $this->influencerId=$value;
    }
    public function setFollowers($value) {
		 $this->followers=$value;
    }
    
}