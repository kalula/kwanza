<?php
class InfluencerCampaign{
    private $follows;
    private $clicks;
    private    $impressions;
    private     $campaign;
    private $influencer;
    private $influencerCampaignId;

    public function getFollows() {
		return $this->follows;
	}
    
        public function getClicks() {
		return $this->clicks;
    }
    
        function getImpressions() {
		return $this->impressions;
    }
    
        function getCampaign() {
		return $this->campaign;
    }
    public function getInfluencer() {
		return $this->influencer;
    }
    public function getInfluencerCampaignId() {
		return $this->influencerCampaignId;
    }
    
    public function setFollows($value) {
		 $this->follows=$value;
	}
 
	public function setClicks($value) {
		 $this->clicks=$value;
    }
    public function setImpressions($value) {
		 $this->impressions=$value;
    }
    public function setCampaign($value) {
		 $this->campaign=$value;
    }
    public function setInfluencer($value) {
		 $this->influencer=$value;
    }
    public function setInfluencerCampaignId($value) {
		 $this->influencerCampaignId=$value;
	}
}