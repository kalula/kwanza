<?php
class Campaign{
    private $name;
    private $description;
    private    $campaignId;
    private     $status;
    private $endDate;
    private $startDate;
    private $image;

    public function getName() {
		return $this->name;
	}    
        public function getDescription() {
		return $this->description;
    }
            function getCampaignId() {
		return $this->campaignId;
    }
    
        function getStatus() {
		return $this->status;
    }
    public function getEndDate() {
		return $this->endDate;
    }
    public function getStartDate() {
		return $this->startDate;
    }
    public function getImage() {
		return $this->image;
    }
    
    public function setName($value) {
		 $this->name=$value;
	}
 
	public function setDescription($value) {
		 $this->description=$value;
    }
    public function setCampaignId($value) {
		 $this->campaignId=$value;
    }
    public function setStatus($value) {
		 $this->status=$value;
    }
    public function setEndDate($value) {
		 $this->endDate=$value;
    }
    public function setStartDate($value) {
		 $this->startDate=$value;
    }
    public function setImage($value) {
        $this->image=$value;
   }
}