<?php
class InfluencerCampaignReport{
    private $follows;
    private $clicks;
    private $impressions;
    private $influencerCampaignId;

    private $campaignName;
	private $campaignDescription;
	private $campaignStartDate;
	private $campaignImage;
	private $campaignEndDate;
	private $campaignStatus;

	private  $influencerFirstName;
	private  $influencerMiddleName;
	private  $influencerLastName;
	private  $influencerAlias;


    public function getFollows() {
		return $this->follows;
	}
    
        public function getClicks() {
		return $this->clicks;
    }
    
        function getImpressions() {
		return $this->impressions;
    }
    
    function getCampaignName() {
		return $this->campaignName;
    }
    function getCampaignDescription() {
		return $this->campaignDescription;
    }
    function getCampaignStartDate() {
		return $this->campaignStartDate;
    }
    function getCampaignImage() {
		return $this->campaignImage;
    }
    function getCampaignEndDate() {
		return $this->campaignEndDate;
    }
    function getCampaignStatus() {
		return $this->campaignStatus;
    }

    public function getInfluencerFirstName() {
		return $this->influencerFirstName;
    }

    public function getInfluencerMiddleName() {
		return $this->influencerMiddleName;
    }

    public function getInfluencerLastName() {
		return $this->influencerLastName;
    }

    public function getInfluencerAlias() {
		return $this->influencerAlias;
    }


    public function getInfluencerCampaignId() {
		return $this->influencerCampaignId;
    }
    
    public function setFollows($value) {
		 $this->follows=$value;
	}
 
	public function setClicks($value) {
		 $this->clicks=$value;
    }
    public function setImpressions($value) {
		 $this->impressions=$value;
    }
    function setCampaignName($value) {
		 $this->campaignName=$value;
    }
    function setCampaignDescription($value) {
		 $this->campaignDescription=$value;
    }
    function setCampaignStartDate($value) {
		 $this->campaignStartDate=$value;
    }
    function setCampaignImage($value) {
		 $this->campaignImage=$value;
    }
    function setCampaignEndDate($value) {
		 $this->campaignEndDate=$value;
    }
    function setCampaignStatus($value) {
		 $this->campaignStatus=$value;
    }

    public function setInfluencerFirstName($value) {
		 $this->influencerFirstName=$value;
    }

    public function setInfluencerMiddleName($value) {
		 $this->influencerMiddleName=$value;
    }

    public function setInfluencerLastName($value) {
		 $this->influencerLastName=$value;
    }

    public function setInfluencerAlias($value) {
		 $this->influencerAlias=$value;
    }

    public function setInfluencerCampaignId($value) {
		 $this->influencerCampaignId=$value;
	}
}