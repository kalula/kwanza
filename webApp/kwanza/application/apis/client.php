<?php
include_once  $_SERVER['DOCUMENT_ROOT'].'/kwanza/application/apis/api.php';
include_once  $_SERVER['DOCUMENT_ROOT'].'/kwanza/application/models/InfluencerCampaign.php';
include_once  $_SERVER['DOCUMENT_ROOT'].'/kwanza/application/models/Campaign.php';
include_once  $_SERVER['DOCUMENT_ROOT'].'/kwanza/application/models/Influencer.php';
include_once  $_SERVER['DOCUMENT_ROOT'].'/kwanza/application/models/UserAccount.php';
include_once  $_SERVER['DOCUMENT_ROOT'].'/kwanza/application/models/InfluencerCampaignReport.php';

function GetInfluencerReport()
{
$get_data = callAPI('GET', 'http://localhost:8097/influencercampaignsreport', false);
$response = $get_data;
//$errors = $response['response']['errors'];
//$data = $response['response']['data'][0];
$campaigns = array();
for ($x = 0; $x < count($response); $x++) {
    $campaign= new InfluencerCampaignReport();
    $campaign->setFollows($response[$x]['follows']);
    $campaign->setClicks($response[$x]['clicks']);
    $campaign->setImpressions($response[$x]['impressions']);
    $campaign->setInfluencerCampaignId($response[$x]['influencerCampaignId']);
    $campaign->setCampaignName($response[$x]['campaignName']);
    $campaign->setCampaignDescription($response[$x]['campaignDescription']);
    $campaign->setCampaignStartDate($response[$x]['campaignStartDate']);
    $campaign->setCampaignImage($response[$x]['campaignImage']);
    $campaign->setCampaignEndDate($response[$x]['campaignEndDate']);
    $campaign->setCampaignStatus($response[$x]['campaignStatus']);
    $campaign->setInfluencerFirstName($response[$x]['influencerFirstName']);
    $campaign->setInfluencerMiddleName($response[$x]['influencerMiddleName']);
    $campaign->setInfluencerLastName($response[$x]['influencerLastName']);
    $campaign->setInfluencerAlias($response[$x]['influencerAlias']);
    $campaigns[$x] = $campaign;
} 
//print_r ($campaigns);
return $campaigns;
}





 function GetInfluencerCampaigns()
{
$get_data = callAPI('GET', 'http://localhost:8097/influencercampaigns', false);
$response = $get_data;
//$errors = $response['response']['errors'];
//$data = $response['response']['data'][0];
$campaigns = array();
for ($x = 0; $x < count($response); $x++) {
    $campaign= new InfluencerCampaign();
    $campaign->setFollows($response[$x]['follows']);
    $campaign->setClicks($response[$x]['clicks']);
    $campaign->setImpressions($response[$x]['impressions']);
    $campaign->setInfluencerCampaignId($response[$x]['influencerCampaignId']);
    $campaign->setCampaign($response[$x]['campaign']);
    $campaign->setInfluencer($response[$x]['influencer']);
    $campaigns[$x] = $campaign;
} 
//print_r ($campaigns);
return $campaigns;
}
function GetCampaigns()
{
$get_data = callAPI('GET', 'http://localhost:8097/campaigns', false);
$response = $get_data;
//$errors = $response['response']['errors'];
//$data = $response['response']['data'][0];
$campaigns = array();
for ($x = 0; $x < count($response); $x++) {
    $campaign= new Campaign();
    $campaign->getName($response[$x]['name']);
    $campaign->getDescription($response[$x]['description']);
    $campaign->getCampaignId($response[$x]['campaignId']);
    $campaign->getStatus($response[$x]['status']);
    $campaign->getEndDate($response[$x]['endDate']);
    $campaign->getStartDate($response[$x]['startDate']);
    $campaign->getImage($response[$x]['image']);
    $campaigns[$x] = $campaign;
} 
//print_r ($campaigns);
return $campaigns;
}
function GetInfluencers()
{
$get_data = callAPI('GET', 'http://localhost:8097/influencers', false);
$response = $get_data;
//$errors = $response['response']['errors'];
//$data = $response['response']['data'][0];
$campaigns = array();
for ($x = 0; $x < count($response); $x++) {
    $campaign= new Influencer();
    $campaign->setUser($response[$x]['user']);
    $campaign->setStatus($response[$x]['status']);
    $campaign->setInfluencerId($response[$x]['influencerId']);
    $campaign->setFollowers($response[$x]['followers']);
    $campaigns[$x] = $campaign;
} 
//print_r ($campaigns);
return $campaigns;
}
function GetUserAccounts()
{
$get_data = callAPI('GET', 'http://localhost:8097/allaccounts', false);
$response = $get_data;
//$errors = $response['response']['errors'];
//$data = $response['response']['data'][0];
$campaigns = array();
for ($x = 0; $x < count($response); $x++) {
    $campaign= new UserAccount();
    $campaign->setUserName($response[$x]['userName']);
    $campaign->setPassword($response[$x]['password']);
    $campaign->setAlias($response[$x]['alias']);
    $campaign->setLastName($response[$x]['lastName']);
    $campaign->setIsActive($response[$x]['isActive']);
    $campaign->setUserId($response[$x]['userId']);
    $campaign->setEmail($response[$x]['email']);
    $campaign->setMiddleName($response[$x]['middleName']);
    $campaign->setDateCreated($response[$x]['dateCreated']);
    $campaign->setFirstName($response[$x]['firstName']);
    $campaigns[$x] = $campaign;
} 
//print_r ($campaigns);
return $campaigns;
}

function GetCampaign($id)
{
    $get_data = callAPI('POST', 'http://localhost:8097/campaigns-id', $id);
$response = $get_data[0];
//$errors = $response['response']['errors'];
//$data = $response['response']['data'][0];

    $campaign= new Campaign();
    $campaign->setName($response[0]['name']);
    $campaign->setDescription($response->description);
    $campaign->setCampaignId($response->campaignId);
    $campaign->setStatus($response->status);
    $campaign->setEndDate($response->endDate);
    $campaign->setStartDate($response->startDate);
    $campaign->setImage($response->image);
//

//print_r ($campaigns);
return $campaign;
}
function GetInfluencer($id)
{
$get_data = callAPI('POST', 'http://localhost:8097/influencer-id', $id);
$response = $get_data;
//$errors = $response['response']['errors'];
//$data = $response['response']['data'][0];
//$campaigns = array();
//for ($x = 0; $x < count($response); $x++) {
    $campaign= new Influencer();
    $campaign->setUser($response->user);
    $campaign->setStatus($response->status);
    $campaign->setInfluencerId($response->influencerId);
    $campaign->setFollowers($response->followers);
 //   $campaigns[$x] = $campaign;
//} 
//print_r ($campaigns);
return $campaign;
}
function GetUserAccount($id)
{
$get_data = callAPI('POST', 'http://localhost:8097/useraccount-id', $id);
$response = $get_data;
//$errors = $response['response']['errors'];
//$data = $response['response']['data'][0];
//$campaigns = array();
//for ($x = 0; $x < count($response); $x++) {
    $campaign= new UserAccount();
    $campaign->setUserName($response->userName);
    $campaign->setPassword($response->password);
    $campaign->setAlias($response->alias);
    $campaign->setLastName($response->lastName);
    $campaign->setIsActive($response->isActive);
    $campaign->setUserId($response->userId);
    $campaign->setEmail($response->email);
    $campaign->setMiddleName($response->middleName);
    $campaign->setDateCreated($response->dateCreated);
    $campaign->setFirstName($response->firstName);
 //   $campaigns[$x] = $campaign;
//} 
//print_r ($campaigns);
return $campaign;
}

